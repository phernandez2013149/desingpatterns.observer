package observer.pattern.demo;

import java.util.*;

class Subject1 implements ISubject{

	List<IObserver> observerList=
		new ArrayList<IObserver>();

	private int myFlag;


	public int getFlag(){
		return myFlag;
	}

	public void setFlag(int myFlag){
		this.myFlag= myFlag;

		notifyObservers(myFlag);
	}




	@Override 
	public void register(IObserver o){
		observerList.add(o);
	}

	@Override
	public void unregister(IObserver o){

		observerList.remove(o);
	}

	@Override
	public void notifyObservers(int updateValue){

		for(int i = 0; i<observerList.size(); i++){
			observerList.get(i).update( 
					this.getClass().getSimpleName()
					,updateValue);
			
		}
	}

}