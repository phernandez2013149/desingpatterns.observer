package observer.pattern.demo;


public class Observer implements IObserver{

	@Override
	public void update(String s, int i){
		System.out.println("Observer: My value3 in subject:"+s+" is now "+ i);
		
	}

}