package observer.pattern.demo;

public class Observer2 implements IObserver{

	@Override
	public void update(String s, int i){
		System.out.println("Observer2: "+
			"my value is change to: "+i+
			" in subject "+s);
	}



}