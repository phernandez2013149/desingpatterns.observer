package observer.pattern.demo;


class ObserverPatternEx{
	

	public static void main (String[] args){


		System.out.println("***Observer pattern demo");
		Observer o = new Observer();
		Observer1 o1 = new Observer1();
		Observer2 o2 = new Observer2();

		Subject sub = new Subject();
		Subject1 sub1 = new Subject1();

		sub.register(o);
		sub.register(o1);

		sub1.register(o1);
		sub1.register(o2);


		System.out.println();
		sub1.setFlag(5);

		System.out.println();
		sub.setFlag(30);

		System.out.println();
		sub1.setFlag(25);

		//unregister
		sub1.unregister(o1);

		System.out.println();
		sub1.setFlag(50);



	}

}