package observer.pattern.demo;

import java.util.*;

class Subject implements ISubject{

	List<IObserver> observerList = 
			new ArrayList<IObserver>();

	private int flag;

	public int getFlag(){
		return flag;
	}

	public void setFlag(int flag){
		this.flag = flag;
		//flag value change .So nofigy 
		//observers(s) 
		notifyObservers(flag);

	}


	@Override 
	public void register(IObserver o){
		observerList.add(o);
	}

	@Override
	public void unregister(IObserver o){

		observerList.remove(o);
	}

	@Override
	public void notifyObservers(int updateValue){

		for(int i = 0; i<observerList.size(); i++){
			observerList.get(i).update( 
					this.getClass().getSimpleName()
					,updateValue);
			
		}
	}
}