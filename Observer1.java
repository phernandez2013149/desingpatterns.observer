package observer.pattern.demo;

public class Observer1 implements IObserver{


	@Override
	public void update(String s,int i){

		System.out.println("Observer1: "+
		 "my value is change in subject: "+s+" to "+ i);
	

	}
}